### Stock Market Dashboard Application
This application is a stock market dashboard designed to provide users with insights into stock prices, allowing them to view graphs, widgets, favorite stocks, and time-wise stock prices. It includes features for user authentication with login and registration functionalities.

## Features
# User Authentication

# Login: Users can log in to the application using their credentials.
Registration: New users can create an account by registering.
Authorization

## Users accessing the dashboard must be logged in.
# Dashboard Overview

# Stock Graphs: Visual representation of stock data through graphs.
# Widgets: Widgets displaying various stock-related information.
Favorites Tab: A section to save and view favorite stocks.
Time-wise Stock Prices: View historical data or real-time updates for stock prices.
Getting Started
To run this application locally, follow these steps:

# Prerequisites
Node.js installed on your machine

## Installation

1 - Clone the repository:
    LINK - https://gitlab.com/alok0888756/stock-market/-/tree/main
    git clone -b main https://gitlab.com/alok0888756/stock-market.git
2 - go to root directory of the project
    cd stock-market
3 - download the required dependencies (node modules)
    npm install
4 - start the application
    npm start

Open your browser and go to http://localhost:3000 to view the application.
Usage
Login: Use existing credentials or register to access the dashboard.
Dashboard: After logging in, explore the stock graphs, widgets, favorite stocks, and time-wise stock price details.
Technologies Used
React.js for frontend development
react-router-dom for routing
Other libraries and dependencies
