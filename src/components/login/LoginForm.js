// LoginForm.js
import React, { useState } from 'react';
import './LoginForm.css'
import { Dashboard } from '../../App';
import { Route } from 'react-router-dom';

const LoginForm = ({ handleLogin }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    // Simulated login logic, replace with actual authentication
    if (username === 'user' && password === 'password') {
      handleLogin({ username }); // Pass user data to handleLogin function
      <Route path="/dashboard" element={<Dashboard username={username}  />} />

    } else {
      alert('Invalid credentials');
    }
  };

  return (
    <div className="form-container">
      <h2>Login</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button type="submit" onClick={handleLogin}>Login</button>
      </form>
    </div>
  );
};

export default LoginForm;
