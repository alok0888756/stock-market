// StockList.js
import React, { useState, useEffect } from 'react';
import StockService from '../services/StockService';
import "./StockList.css"

const StockList = () => {
  const [stocks, setStocks] = useState([]);

  useEffect(() => {
    const fetchStockList = async () => {
      try {
        const stockList = await StockService.getStockList();
        setStocks(stockList);
      } catch (error) {
        // Handle error
      }
    };

    fetchStockList();
  }, []);

  return (
    <div className="stock-list">
      <h2>Stock List</h2>
      <ul>
        {stocks.map(stock => (
          <li key={stock.symbol}>
            <strong>{stock.symbol}</strong> - {stock.name}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default StockList;
