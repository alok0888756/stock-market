import axios from 'axios';

const API_KEY = 'sk_1edb5d35d9ba4db1877dacc9a8950833';


const StockService = {
  getStockList: async () => {
    try {
      const response = await axios.get(`https://cloud.iexapis.com/stable/ref-data/symbols?token=${API_KEY}`);
      return response.data;
    } catch (error) {
      console.error('Error fetching stock list:', error);
      throw error;
    }
  },
};

export default StockService;